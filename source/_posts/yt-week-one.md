---
title: Bonjourno
date: 2024-02-12 21:33:00
tags:
---

A week ago, I posted to Youtube for the first time. I planned to post once or twice a week and suppliment with shorts.

## The Diary
### Day 1
Two shorts videos posted today. They were both with Cat Janice's song *name here* which she gave to her son before the cancer kills her. The first video was drawing cute lunar new year icons, and the second was after practicing drawing cute cartoon faces.
### Day 2
Two more shorts today. One was Shane (Yaw) Dawson's face and the second was a relaxing green and purple page.
### Day 3
OC Generator day. I was inspired to draw, but didn't feel like being creative. Some were cute. I drew the results in the style of the OP's result.
### Day 4
New Sketchbook post showing the inside covers I do for my sketchbooks (acrylic markers). This post is my most interacted with post yet.

After a nap, I got hit be a rather large second wind, so I drew John Oliver until I got distracted by his hands an spent the new two hours pausing the videos and drawing his gestures.
### Day 5
Pencil crayons collection organistion timelapse. First time editing a video in a long while, but since I don't have an international phone number, I had to speed it up to a minute. It was 2.5 hrs -> 26 minutes -> 1 minute. Fortunately, I went to test upload before doing the voice over! Need to find a fix for this situation. For now, aparently I am limited to under five minutes, but I'll need to test this too because the friend I spoke to seemed to be guessing.
### Day 6
Another OC generator. I liked the results this time. My gel pen is pretty gross, so I may get anime white ink so it sits on top better. Idea: try acrylic white paint and the white acrylic marker too.
### Day 7



## Stats Breakdown 
The following is all by day...
