---
title: GitHub to GitLab Thoughts
date: 2024-01-23 14:41:50
tags:
---


## Thoughts: Github to GitLab

### TL;DR

I like using the command prompt, so it makes up for the inconvenience of redoing my site. I have been enjoying the experience. My understanding of websites and such have drastically increased over the past seven years. Who'd'a thunk?

### Journal

2024-01-21 | Day 1
In the interest of transparency, I didn't make this choice. Github has stopped working, so I am assuming that China has banned it until further notice. I had made a Gitlab account 8 months before to backup my github repositories because I had a feeling this exact situation would happen, and here we are. In the future, I should probably back currently used repos up every few days, because the big project I had spent most days working on is two months behind. I'll have to figure this mess out. I started using Hexo this time because Jekyll was the one Github seemed to suggest. I don't remember Jekkyl enough to make a meaningful comparison.
|
|
|
2023-01-23 | Day 4
For now, I am happy to make a static site to replace the awful mess I built years ago. So far, so good. I was struggling a little, but now I have found my footing. And, it has been quite fun to learn how to perform more tasks with cmd. I like using the tool, so it has been quite exciting to practice new skills there. I think I may stick to Hexo as my go-to static site generator unless the other big ones support this functionality. I have decided on a neon, sci-fi, 80s or 90's high-tech feel, but also minimalistic and clean. Imagine like clean modern meets neon city in a sci-fi. I have been working on the graphics on paper, so the graphics currently in use are the Landscape theme's defaults.

Also, I like the "pipelines" and their checkmarks. May be a Chinese ISP thing, but I swear Gitlab commits much faster than Github ever did. Although I am sure the extra step of communicating through the "GitHub Desktop" program added some delay to the handshakes.