---
title: Youtube And BiliBili
date: 2024-02-16 19:03:23
tags:
---

I am looking into making a personal browser home page with links to my usual sites, the weather and triple clock, my social profiles with their sub counts and icons for whether it has notifications, and the github+gitlab contributions chart. Maybe it would be more suited as a desktop wallpaper. I would be down with it being the automatic screensaver for my computer, but idk how it even start toward that. Also, screensavers never work right.

It has occured to me that I couldn't have my yt counter without a vpn, I doubt I will find a way around that one.

<iframe height="90px" width="310px" frameborder="0" src="https://socialcounts.org/youtube-live-subscriber-count/UC4mMdZlyEiYeg5Wu3T61BvQ/embed" allowFullScreen></iframe>

<iframe height="90px" width="310px" frameborder="0" src="https://socialcounts.org/youtube-live-subscriber-count/UC4mMdZlyEiYeg5Wu3T61BvQ/embed?counter=0&fullscreen=true" allowFullScreen></iframe>


<div class="list_id__z9YyF">18</div>

https://socialcounts.org/youtube-live-subscriber-count/UCYk3T4ZskjEJ3VLMyhWK6yg  


![YT sub counts (subs)](https://socialcounts.org/youtube-live-subscriber-count/UCYk3T4ZskjEJ3VLMyhWK6yg/embed?counter=0 "Mia Seym YT subs count")

![YT sub counts (views)](https://socialcounts.org/youtube-live-subscriber-count/UCYk3T4ZskjEJ3VLMyhWK6yg/embed?counter=1 "Mia Seym YT subs count")

<iframe height="90px" width="310px" frameborder="0" src="https://socialcounts.org/youtube-live-subscriber-count/UCYk3T4ZskjEJ3VLMyhWK6yg/embed?counter=0" allowFullScreen></iframe>

<iframe height="90px" width="310px" frameborder="0" src="https://socialcounts.org/youtube-live-subscriber-count/UCYk3T4ZskjEJ3VLMyhWK6yg/embed?counter=1" allowFullScreen></iframe>


![Bilibili sub count](https://teiresa.github.io/responsive-site/strap/bilibili/subs/mia_api.html "Mia Seym Bilibili subs count")

<iframe height="90px" width="310px" frameborder="0" src="https://teiresa.github.io/responsive-site/strap/bilibili/subs/mia_api.html"></iframe>