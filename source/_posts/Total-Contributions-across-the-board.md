---
title: Total Contributions across the board
date: 2024-01-23 16:26:52
tags:
---


<img src="http://www.allgitcontributiongraph.com/justgraphit.svg?gitlab=teiResa&github=teiResa">

![GH GL contributions](http://www.allgitcontributiongraph.com/justgraphit.svg?gitlab=teiResa&github=teiResa "GH and GL combined contributions table")

https://www.allgitcontributiongraph.com/

I will come back to this later. It's not very urgent, so I will add it to my "motivated but don't want start something new" list. I'm sure this will all seem obvious with some distance.

Explaination: This is the contributions chart from the gh and gl profiles, but together. This way I can feel good about my number of contributions while github is down in China. I would like to also put this on my browser "dashboard", the home page I will eventually make for my broswers across all devices.N