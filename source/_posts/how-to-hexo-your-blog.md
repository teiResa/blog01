---
title: how to hexo your blog
date: 2024-01-22 15:20:03
tags:
---


## How to hexo your blog

### Resources followed
<ul>
<li><a href="https://hexo.io/docs/">hexo.io</a></li>
<li><a href="https://bogomolov.tech/how-this-blog-created/">this tutorial which was helpful</a></li>
</ul>


For blog01, remember that it is within a folder also named blog01, so write this twice (This post is located at ~\blog01\blog01\source\_posts\how-to-hexo-your-blog.md)

``` bash
 cd blog01
```

Then you can continue with the

``` bash
 hexo new "TITLE OF NEW POST"
```

To preview the post, (ctrl click to open the live preview in browser; ctrl+c when done)
``` bash
 hexo server
```

And for committing

``` bash
git add --all
git commit -m "Commit message, describing your changes. It is required, or the commit is cancelled."
```